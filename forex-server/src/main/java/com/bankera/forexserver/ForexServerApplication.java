package com.bankera.forexserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.money.convert.ExchangeRateProvider;
import javax.money.convert.ExchangeRateProviderSupplier;
import javax.money.convert.MonetaryConversions;
import java.util.Collection;
import java.util.List;

@SpringBootApplication
public class ForexServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ForexServerApplication.class, args);
    }

}
