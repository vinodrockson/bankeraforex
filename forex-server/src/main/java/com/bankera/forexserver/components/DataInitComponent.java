package com.bankera.forexserver.components;

import com.bankera.forexserver.exceptions.UserNotFoundException;
import com.bankera.forexserver.models.Authority;
import com.bankera.forexserver.models.BfxCurrency;
import com.bankera.forexserver.models.CurrencyRate;
import com.bankera.forexserver.models.User;
import com.bankera.forexserver.services.currencyrate.CurrencyRateService;
import com.bankera.forexserver.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.bankera.forexserver.utils.Constants.BankeraCurrency.*;
import static com.bankera.forexserver.utils.Constants.Security.DEFAULT_USER_ADMIN_EMAIL;
import static com.bankera.forexserver.utils.Constants.Security.DEFAULT_USER_ADMIN_PASSWORD;

/**
 * Component to initialize data on startup
 *
 * @author Vinod John
 */
@Component
public class DataInitComponent {
    @Autowired
    private UserService userService;

    @Autowired
    private CurrencyRateService currencyRateService;

    @PostConstruct
    public void initUserAndAuthorityData() {
        try {
            userService.getUserByEmail(DEFAULT_USER_ADMIN_EMAIL);
        } catch (UserNotFoundException e) {
            User user = new User();
            user.setEmail(DEFAULT_USER_ADMIN_EMAIL);
            user.setPassword(DEFAULT_USER_ADMIN_PASSWORD);
            user.setActive(true);
            user.setEmailActivated(true);
            user.setAuthority(Authority.ROLE_ADMIN.name());
            userService.createUser(user);
        }
    }

    // For testing purposes
    @PostConstruct
    public void initCurrencyRateData() {
        Map<String, Double> currencyRates = Map.of("EUR", 1D, "USD", 0.809552722, "GBP", 1.126695,
                BTC, 6977.089657, ETH, 685.2944747, FKE, 0.025);
        List<CurrencyRate> currencyRateList = new ArrayList<>();

        currencyRates.forEach((currencyCode, rate) -> {
            BfxCurrency currency = new BfxCurrency();
            currency.setCurrencyCode(currencyCode);

            CurrencyRate currencyRate = new CurrencyRate();
            currencyRate.setCurrency(currency);
            currencyRate.setExchangeRate(rate);
            currencyRateList.add(currencyRate);
        });

        currencyRateService.createCurrencyRate(currencyRateList);
    }
}

