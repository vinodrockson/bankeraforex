package com.bankera.forexserver.components;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Component for Token Properties
 *
 * @author Vinod John
 */
@Data
@Component
@ConfigurationProperties(prefix = "security.jwt")
public class SecurityTokenProperties {
    @Value("${jwt.token.login-path}")
    private String loginPath;

    @Value("${jwt.token.header}")
    private String header;

    @Value("${jwt.token.prefix}")
    private String prefix;

    @Value("${jwt.token.expiration-seconds}")
    private int expiration = 86400;

    @Value("${jwt.token.secret}")
    private String secret = "JwtSecretKey";
}
