package com.bankera.forexserver.configurations.security;

import com.bankera.forexserver.components.SecurityTokenProperties;
import com.bankera.forexserver.models.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A custom filter for authentication
 *
 * @author Vinod John
 */
public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private final ObjectMapper objectMapper;
    private final AuthenticationManager authenticationManager;
    private final SecurityTokenProperties securityTokenProperties;

    public AuthenticationFilter(AuthenticationManager authenticationManager, SecurityTokenProperties securityTokenProperties) {
        this.authenticationManager = authenticationManager;
        this.securityTokenProperties = securityTokenProperties;
        objectMapper = new ObjectMapper();
        setLoginPath(securityTokenProperties);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        try {
            User user = getUser(request);
            UsernamePasswordAuthenticationToken token = createAuthenticationToken(user);
            return authenticationManager.authenticate(token);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication auth) {
        response.addHeader(securityTokenProperties.getHeader(), securityTokenProperties.getPrefix() + createToken(auth));
    }

    // PRIVATE METHODS //
    private void setLoginPath(SecurityTokenProperties securityTokenProperties) {
        setRequiresAuthenticationRequestMatcher(
                new AntPathRequestMatcher(securityTokenProperties.getLoginPath(), "POST"));
    }

    private String createToken(Authentication auth) {
        long now = System.currentTimeMillis();
        List<String> authorities = auth.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        return Jwts.builder()
                .setSubject(auth.getName())
                .claim("authorities", authorities)
                .setIssuedAt(new Date(now))
                .setExpiration(new Date(now + securityTokenProperties.getExpiration() * 1000))
                .signWith(Keys.hmacShaKeyFor(securityTokenProperties.getSecret().getBytes()), SignatureAlgorithm.HS512)
                .compact();
    }

    private User getUser(HttpServletRequest request) throws IOException {
        return objectMapper.readValue(request.getInputStream(), User.class);
    }

    private UsernamePasswordAuthenticationToken createAuthenticationToken(User user) {
        return new UsernamePasswordAuthenticationToken(
                user.getEmail(),
                user.getPassword(),
                Collections.emptyList()
        );
    }
}
