package com.bankera.forexserver.configurations.security;

import com.bankera.forexserver.components.SecurityTokenProperties;
import com.bankera.forexserver.models.Authority;
import com.bankera.forexserver.services.userdetails.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletResponse;

/**
 * Configuration for security
 *
 * @author Vinod John
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private SecurityTokenProperties securityTokenProperties;

    @Bean
    public UserDetailsService userDetailsService() {
        return new CustomUserDetailsService();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService());
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        String role = "ROLE_",
                admin = Authority.ROLE_ADMIN.name().replace(role, ""),
                user = Authority.ROLE_USER.name().replace(role, "");

        http.authorizeRequests()
                .antMatchers( "/user/create", "/user/activate/*", "/user/reset-password-generate/*",
                        "/user/reset-password/*", "/currency/active", "/exchange")
                .permitAll()
                .antMatchers("/user", "/user/delete/*", "/user/restore/*")
                .hasRole(admin)
                .antMatchers("/auth/login", "/user/*", "/user/update/*", "/currency-rate", "/currency",
                        "/currency/delete/*", "/currency/restore/*")
                .hasAnyRole(admin, user)
                .and()
                .httpBasic()
                .and()
                .logout().permitAll(false)
                .and()
                .addFilter(new AuthenticationFilter(authenticationManagerBean(), securityTokenProperties))
                .addFilterAfter(new AuthorizationFilter(securityTokenProperties), UsernamePasswordAuthenticationFilter.class)
                .cors()
                .and()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling().authenticationEntryPoint((request, response, authException) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED));
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers("/h2-console/**");
    }
}
