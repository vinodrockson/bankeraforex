package com.bankera.forexserver.controllers;

import com.bankera.forexserver.dtos.BfxCurrencyDto;
import com.bankera.forexserver.dtos.BfxCurrencyShortDto;
import com.bankera.forexserver.exceptions.CurrencyNotFoundException;
import com.bankera.forexserver.models.BfxCurrency;
import com.bankera.forexserver.services.currency.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Controller to handle currency requests
 *
 * @author Vinod John
 */
@RestController
@RequestMapping("/currency")
public class CurrencyController {
    @Autowired
    private CurrencyService currencyService;

    @GetMapping
    public List<BfxCurrencyDto> getAllCurrencies() {
        return currencyService.getAllCurrencies().stream()
                .map(BfxCurrency::currencyDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/active")
    public List<BfxCurrencyShortDto> getActiveCurrencies() {
        return currencyService.getActiveCurrencies().stream()
                .map(BfxCurrency::currencyShortDto)
                .collect(Collectors.toList());
    }

    @GetMapping("/delete/{id}")
    public ResponseEntity<?> deleteCurrency(@PathVariable("id") UUID id) throws CurrencyNotFoundException {
        currencyService.deleteCurrencyById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/restore/{id}")
    public ResponseEntity<?> restoreCurrency(@PathVariable("id") UUID id) throws CurrencyNotFoundException {
        currencyService.restoreCurrencyById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
