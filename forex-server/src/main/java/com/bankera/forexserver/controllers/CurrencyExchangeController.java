package com.bankera.forexserver.controllers;

import com.bankera.forexserver.models.CurrencyExchange;
import org.javamoney.moneta.Money;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.money.MonetaryAmount;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.ExchangeRateProvider;
import javax.money.convert.MonetaryConversions;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Controller to handle Currency Exchange requests
 *
 * @author Vinod John
 */
@RestController
@RequestMapping("/exchange")
public class CurrencyExchangeController {
    @PostMapping
    public ResponseEntity<?> getExchange(@Valid @RequestBody CurrencyExchange currencyExchange) {
        ExchangeRateProvider bankeraExchangeRateProvider = MonetaryConversions.getExchangeRateProvider("BANKERA");
        CurrencyConversion currencyConversion = bankeraExchangeRateProvider
                .getCurrencyConversion(currencyExchange.getFinalCurrencyCode());
        MonetaryAmount initialMoney = Money.of(currencyExchange.getQuantity(), currencyExchange.getInitialCurrencyCode());
        MonetaryAmount finalMoney = initialMoney.with(currencyConversion);
        BigDecimal result = BigDecimal.valueOf(finalMoney.getNumber().doubleValueExact())
                .setScale(18, RoundingMode.FLOOR).stripTrailingZeros();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
