package com.bankera.forexserver.controllers;

import com.bankera.forexserver.exceptions.InvalidCurrencyRateException;
import com.bankera.forexserver.exceptions.InvalidFileException;
import com.bankera.forexserver.models.BfxCurrency;
import com.bankera.forexserver.models.CurrencyRate;
import com.bankera.forexserver.services.currencyrate.CurrencyRateService;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.money.Monetary;
import javax.money.UnknownCurrencyException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller to handle Currency Rate requests
 *
 * @author Vinod John
 */
@RestController
@RequestMapping("/currency-rate")
public class CurrencyRateController {
    public static final String FILE_CONTENT_TYPE = "text/csv";
    public static final String FILE_FORMAT = "CSV";

    @Autowired
    private CurrencyRateService currencyRateService;

    @PostMapping
    public ResponseEntity<?> uploadCurrencyRateCSV(@RequestParam("file") MultipartFile file)
            throws IOException, InvalidFileException, InvalidCurrencyRateException {
        if (null == file || file.isEmpty()) {
            throw new FileNotFoundException("Empty file!");
        } else if (!FILE_CONTENT_TYPE.equals(file.getContentType())) {
            throw new InvalidFileException(file.getOriginalFilename(), FILE_FORMAT);
        }

        InputStreamReader reader = new InputStreamReader(file.getInputStream());
        CSVParser records = new CSVParser(reader, CSVFormat.DEFAULT);
        List<CurrencyRate> currencyRates = new ArrayList<>();

        for (CSVRecord csvRecord : records) {
            if (!Monetary.isCurrencyAvailable(csvRecord.get(0))) {
                throw new UnknownCurrencyException("Unknown currency: " + csvRecord.get(0));
            }

            if (csvRecord.get(1).isBlank()) {
                throw new InvalidCurrencyRateException(csvRecord.get(0), csvRecord.get(1));
            }

            String rate = !csvRecord.get(1).contains(".") && csvRecord.size() > 2 ?
                    csvRecord.get(1) + '.' + csvRecord.get(2) : csvRecord.get(1);

            BfxCurrency currency = new BfxCurrency();
            currency.setCurrencyCode(csvRecord.get(0));

            CurrencyRate currencyRate = new CurrencyRate();
            currencyRate.setCurrency(currency);
            currencyRate.setExchangeRate(Double.parseDouble(rate));
            currencyRates.add(currencyRate);
        }

        currencyRateService.createCurrencyRate(currencyRates);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
