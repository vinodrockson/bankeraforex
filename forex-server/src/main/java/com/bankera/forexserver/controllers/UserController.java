package com.bankera.forexserver.controllers;

import com.bankera.forexserver.exceptions.ExternalSessionExpiredException;
import com.bankera.forexserver.exceptions.ExternalSessionNotFoundException;
import com.bankera.forexserver.exceptions.UserNotFoundException;
import com.bankera.forexserver.models.User;
import com.bankera.forexserver.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

/**
 * Controller to handle User requests
 *
 * @author VinodJohn
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable("id") UUID id) throws UserNotFoundException {
        return userService.getUserById(id);
    }

    @PostMapping("/create")
    public ResponseEntity<User> createUser(@Valid @RequestBody User user) {
        userService.createUser(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @PostMapping("/update/{id}")
    public ResponseEntity<User> updateUser(@Valid @RequestBody User user, @PathVariable("id") UUID id)
            throws UserNotFoundException {
        user.setId(id);
        userService.updateUser(user);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping("/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") UUID id) throws UserNotFoundException {
        userService.deleteUserById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/restore/{id}")
    public ResponseEntity<?> restoreUser(@PathVariable("id") UUID id) throws UserNotFoundException {
        userService.restoreUserById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/activate/{sessionId}")
    public ResponseEntity<?> activateUser(@PathVariable("sessionId") UUID sessionId) throws
            ExternalSessionNotFoundException, ExternalSessionExpiredException {
        userService.activateUser(sessionId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/reset-password-generate/{id}")
    public ResponseEntity<?> resetPasswordGenerate(@PathVariable("id") String email) throws UserNotFoundException {
        userService.resetUserPasswordByEmail(email);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/reset-password/{sessionId}")
    public ResponseEntity<?> resetPassword(@PathVariable("sessionId") UUID sessionId,
                                           @Valid @Size(min = 6, max = 25, message = "{messages.constraints.password-length}")
                                           @RequestBody String password)
            throws ExternalSessionNotFoundException, ExternalSessionExpiredException {
        userService.resetUserPassword(sessionId, password);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
