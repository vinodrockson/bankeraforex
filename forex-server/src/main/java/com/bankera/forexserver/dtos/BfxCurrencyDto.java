package com.bankera.forexserver.dtos;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

/**
 * Currency Dto
 *
 * @author Vinod John
 */
@Getter
@Builder
public class BfxCurrencyDto {
    private final UUID id;
    private final String code;
    private final String name;
    private final String symbol;
    private final boolean active;
}
