package com.bankera.forexserver.dtos;

import lombok.Builder;
import lombok.Getter;

/**
 * Currency DTO with minimal currency fields
 *
 * @author Vinod John
 */
@Getter
@Builder
public class BfxCurrencyShortDto {
    private final String code;
    private final String name;
    private final String symbol;
}
