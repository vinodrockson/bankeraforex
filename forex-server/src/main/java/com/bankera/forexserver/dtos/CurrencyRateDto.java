package com.bankera.forexserver.dtos;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

/**
 * Currency Rate DTO
 *
 * @author Vinod John
 */
@Getter
@Builder
public class CurrencyRateDto {
    private final String currencyCode;
    private final Double exchangeRate;
    private final LocalDateTime createdDate;
}
