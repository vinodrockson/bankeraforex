package com.bankera.forexserver.exceptions;

import java.util.UUID;

/**
 * Exception to handle Currency availability
 *
 * @author Vinod John
 */
public class CurrencyNotFoundException extends Exception {
    private static final long serialVersionUID = 1L;

    public CurrencyNotFoundException(String currencyCode) {
        super("Currency (Code: " + currencyCode + ") not found!");
    }

    public CurrencyNotFoundException(UUID id) {
        super("Currency (id: " + id.toString() + ") not found!");
    }
}
