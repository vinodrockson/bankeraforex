package com.bankera.forexserver.exceptions;


public class ExternalSessionExpiredException extends Exception {
    private static final long serialVersionUID = 1L;

    public ExternalSessionExpiredException() {
        super("Link has been expired! A new link has been sent to your email.");
    }
}
