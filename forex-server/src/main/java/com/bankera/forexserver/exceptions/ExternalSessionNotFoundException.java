package com.bankera.forexserver.exceptions;

/**
 * Exception to handle ExternalSession's unavailability
 *
 * @author VinodJohn
 */
public class ExternalSessionNotFoundException extends Exception {
    private static final long serialVersionUID = 1L;

    public ExternalSessionNotFoundException() {
        super("Session not found!");
    }
}
