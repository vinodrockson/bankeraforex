package com.bankera.forexserver.exceptions;

/**
 * Exception to handle Currency Rate type
 *
 * @author Vinod John
 */
public class InvalidCurrencyRateException extends Exception {
    private static final long serialVersionUID = 1L;

    public InvalidCurrencyRateException(String currency, String rate) {
        super("Invalid exchange rate (" + rate + ") for currency (" + currency + ")!");
    }
}
