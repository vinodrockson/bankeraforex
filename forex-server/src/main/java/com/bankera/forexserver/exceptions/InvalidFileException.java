package com.bankera.forexserver.exceptions;

/**
 * Exception to handle invalid file
 *
 * @author Vinod John
 */
public class InvalidFileException extends Exception {
    private static final long serialVersionUID = 1L;

    public InvalidFileException(String fileName, String fileType) {
        super("Invalid file format! File: " + fileName + ". Valid file format is " + fileType);
    }
}
