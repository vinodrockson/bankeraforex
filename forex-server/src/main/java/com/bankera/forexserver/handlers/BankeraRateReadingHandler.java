package com.bankera.forexserver.handlers;

import com.bankera.forexserver.dtos.CurrencyRateDto;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.javamoney.moneta.convert.ExchangeRateBuilder;
import org.javamoney.moneta.spi.DefaultNumberValue;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.convert.ConversionContextBuilder;
import javax.money.convert.ExchangeRate;
import javax.money.convert.ProviderContext;
import javax.money.convert.RateType;
import java.io.FileReader;
import java.io.Reader;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static com.bankera.forexserver.utils.Constants.BankeraCurrency.BASE_CURRENCY;

/**
 * Handler to read and parse currency rate data
 *
 * @author Vinod John
 */
public class BankeraRateReadingHandler {
    private final Map<LocalDate, Map<String, ExchangeRate>> historicRates;

    private final ProviderContext context;

    public BankeraRateReadingHandler(Map<LocalDate, Map<String, ExchangeRate>> historicRates, ProviderContext context) {
        this.historicRates = historicRates;
        this.context = context;
        getRates();
    }

    private void getRates() {
        List<CurrencyRateDto> currencyRates = getCurrencyRates();

        currencyRates.forEach(currencyRate -> addRate(Monetary.getCurrency(currencyRate.getCurrencyCode()),
                currencyRate.getCreatedDate().toLocalDate(), currencyRate.getExchangeRate()));
    }

    private void addRate(CurrencyUnit term, LocalDate localDate, Number rate) {
        RateType rateType = RateType.HISTORIC;
        ExchangeRateBuilder builder;

        if (Objects.nonNull(localDate)) {
            if (localDate.equals(LocalDate.now())) {
                rateType = RateType.DEFERRED;
            }

            builder = new ExchangeRateBuilder(ConversionContextBuilder.create(context, rateType).set(localDate).build());
        } else {
            builder = new ExchangeRateBuilder(ConversionContextBuilder.create(context, rateType).build());
        }

        builder.setBase(BASE_CURRENCY);
        builder.setTerm(term);
        builder.setFactor(DefaultNumberValue.of(rate));
        ExchangeRate exchangeRate = builder.build();
        Map<String, ExchangeRate> rateMap = this.historicRates.get(localDate);

        if (Objects.isNull(rateMap)) {
            synchronized (this.historicRates) {
                rateMap = Optional.ofNullable(this.historicRates.get(localDate)).orElse(new ConcurrentHashMap<>());
                this.historicRates.putIfAbsent(localDate, rateMap);

            }
        }

        rateMap.put(term.getCurrencyCode(), exchangeRate);
    }

    private List<CurrencyRateDto> getCurrencyRates() {
        List<CurrencyRateDto> currencyRateList = new ArrayList<>();

        try {
            Reader reader = new FileReader("forex-server/src/main/resources/bankera-rates.json");
            Gson gson = new GsonBuilder().create();
            CurrencyRateDto[] currencyRates = gson.fromJson(reader, CurrencyRateDto[].class);
            currencyRateList = Arrays.asList(currencyRates);
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return currencyRateList;
    }
}
