package com.bankera.forexserver.models;

/**
 * Authority values
 *
 * @author VinodJohn
 */

public enum Authority {
    ROLE_ADMIN, ROLE_USER
}
