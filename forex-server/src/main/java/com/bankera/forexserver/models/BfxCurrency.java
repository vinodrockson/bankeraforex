package com.bankera.forexserver.models;

import com.bankera.forexserver.dtos.BfxCurrencyDto;
import com.bankera.forexserver.dtos.BfxCurrencyShortDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.UUID;

/**
 * Currency model
 *
 * @author Vinod John
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class BfxCurrency extends Auditable<String> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @JsonIgnore
    @GeneratedValue(generator = "UUID")
    @Column(updatable = false, nullable = false)
    @Type(type = "org.hibernate.type.UUIDCharType")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    private String currencyCode;

    private String currencyName;

    private String currencySymbol;

    private boolean isActive;

    public BfxCurrencyShortDto currencyShortDto() {
        return BfxCurrencyShortDto.builder()
                .code(currencyCode)
                .name(currencyName)
                .symbol(currencySymbol)
                .build();
    }

    public BfxCurrencyDto currencyDto() {
        return BfxCurrencyDto.builder()
                .id(id)
                .code(currencyCode)
                .name(currencyName)
                .symbol(currencySymbol)
                .active(isActive)
                .build();
    }
}
