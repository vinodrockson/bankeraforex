package com.bankera.forexserver.models;

import com.bankera.forexserver.utils.constraints.ValidCurrency;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

/**
 * Currency Exchange model
 *
 * @author Vinod John
 */
@Data
public class CurrencyExchange {
    @NotBlank(message = "{messages.constraints.invalid-initial-currency}")
    @ValidCurrency
    private String initialCurrencyCode;

    @NotBlank(message = "{messages.constraints.invalid-final-currency}")
    @ValidCurrency
    private String finalCurrencyCode;

    @NotNull(message = "{messages.constraints.null-quantity}")
    @Positive(message = "{messages.constraints.positive-quantity}")
    private Double quantity;
}
