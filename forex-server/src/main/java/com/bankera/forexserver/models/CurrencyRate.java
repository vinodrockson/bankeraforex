package com.bankera.forexserver.models;

import com.bankera.forexserver.dtos.CurrencyRateDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

/**
 * Currency Rate model
 *
 * @author Vinod John
 */
@Data
@Entity
@EqualsAndHashCode(callSuper = true)
public class CurrencyRate extends Auditable<String> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @JsonIgnore
    @GeneratedValue(generator = "UUID")
    @Column(updatable = false, nullable = false)
    @Type(type = "org.hibernate.type.UUIDCharType")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @OneToOne(fetch = FetchType.EAGER)
    private BfxCurrency currency;

    private Double exchangeRate;

    public CurrencyRateDto currencyRateDto() {
        return CurrencyRateDto.builder()
                .currencyCode(currency.getCurrencyCode())
                .exchangeRate(exchangeRate)
                .createdDate(createdDate)
                .build();
    }
}
