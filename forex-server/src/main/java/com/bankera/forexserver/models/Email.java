package com.bankera.forexserver.models;

import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * Email model
 *
 * @author VinodJohn
 */
@Data
public class Email implements Serializable {
    private static final long serialVersionUID = 1L;

    private String recipient;
    private String subject;
    private String message;
    private Map<String, String> links;
}
