package com.bankera.forexserver.models;

public enum ExternalSessionType {
    CREATE_USER, RESET_USER_PASSWORD
}
