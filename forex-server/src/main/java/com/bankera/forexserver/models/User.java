package com.bankera.forexserver.models;

import com.bankera.forexserver.utils.constraints.UniqueUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.UUID;

/**
 * User model
 *
 * @author VinodJohn
 */
@Entity
@Data
@UniqueUser
@EqualsAndHashCode(callSuper = true)
public class User extends Auditable<String> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @JsonIgnore
    @GeneratedValue(generator = "UUID")
    @Column(updatable = false, nullable = false)
    @Type(type = "org.hibernate.type.UUIDCharType")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @Column(unique = true)
    @Email(message = "{messages.constraints.invalid-email}")
    private String email;

    @NotBlank(message = "{messages.constraints.blank-password}")
    @Size(min = 6, max = 25, message = "{messages.constraints.password-length}")
    private String password;

    @JsonIgnore
    private String authority;

    @JsonIgnore
    private boolean isEmailActivated;

    @JsonIgnore
    private boolean isActive;
}
