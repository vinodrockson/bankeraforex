package com.bankera.forexserver.providers;

import org.javamoney.moneta.CurrencyUnitBuilder;

import javax.money.CurrencyContext;
import javax.money.CurrencyContextBuilder;
import javax.money.CurrencyQuery;
import javax.money.CurrencyUnit;
import javax.money.spi.CurrencyProviderSpi;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static com.bankera.forexserver.utils.Constants.BankeraCurrency.*;

/**
 * Implementation of Currency Provider to add custom currencies
 *
 * @author Vinod John
 */
public final class BankeraCurrencyProvider implements CurrencyProviderSpi {
    private Set<CurrencyUnit> currencySet = new HashSet<>();

    public BankeraCurrencyProvider() {
        CurrencyContext context = CurrencyContextBuilder.of("Bankera").build();
        currencySet.add(CurrencyUnitBuilder.of(BTC, context).setDefaultFractionDigits(6).build(true));
        currencySet.add(CurrencyUnitBuilder.of(ETH, context).setDefaultFractionDigits(6).build(true));
        currencySet.add(CurrencyUnitBuilder.of(FKE, context).setDefaultFractionDigits(6).build(true));
        currencySet = Collections.unmodifiableSet(currencySet);
    }


    @Override
    public Set<CurrencyUnit> getCurrencies(CurrencyQuery query) {
        if (Boolean.TRUE.equals(query.getBoolean("Bankera"))) {
            return currencySet;
        }

        return Collections.emptySet();
    }
}
