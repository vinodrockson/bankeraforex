package com.bankera.forexserver.providers;

import com.bankera.forexserver.handlers.BankeraRateReadingHandler;
import org.javamoney.moneta.convert.ExchangeRateBuilder;
import org.javamoney.moneta.spi.AbstractRateProvider;
import org.javamoney.moneta.spi.DefaultNumberValue;

import javax.money.Monetary;
import javax.money.MonetaryException;
import javax.money.convert.*;
import java.math.MathContext;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.bankera.forexserver.utils.Constants.BankeraCurrency.BASE_CURRENCY_CODE;

/**
 * This class implements an {@link javax.money.convert.ExchangeRateProvider} that loads data from
 * the Bankera service. It loads the current exchange rates.
 *
 * @author Vinod John
 */
public class BankeraRateProvider extends AbstractRateProvider {
    /**
     * The {@link javax.money.convert.ConversionContext} of this provider.
     */
    private static final ProviderContext CONTEXT = ProviderContextBuilder.of("BANKERA", RateType.OTHER)
            .set("providerDescription", "Bankera Rate Provider")
            .build();
    /**
     * Historic exchange rates, rate timestamp as UTC long.
     */
    protected Map<LocalDate, Map<String, ExchangeRate>> rates = new ConcurrentHashMap<>();

    /**
     * Constructor, also loads initial data
     */
    public BankeraRateProvider() {
        super(CONTEXT);
    }

    @Override
    public ExchangeRate getExchangeRate(ConversionQuery conversionQuery) {
        Objects.requireNonNull(conversionQuery);
        new BankeraRateReadingHandler(rates, CONTEXT);

        if (rates.isEmpty()) {
            return null;
        }

        RateResult result = findExchangeRate(conversionQuery);
        ExchangeRateBuilder builder = getBuilder(conversionQuery);
        ExchangeRate sourceRate = result.targets.get(conversionQuery.getBaseCurrency()
                .getCurrencyCode());
        ExchangeRate target = result.targets
                .get(conversionQuery.getCurrency().getCurrencyCode());
        return createExchangeRate(conversionQuery, builder, sourceRate, target);
    }

    @Override
    public String toString() {
        return getClass().getName() + '{' +
                " context: " + CONTEXT + '}';
    }

    // PRIVATE METHODS //
    private RateResult findExchangeRate(ConversionQuery conversionQuery) {
        LocalDate[] dates = getQueryDates(conversionQuery);

        if (null == dates) {
            Comparator<LocalDate> comparator = Comparator.naturalOrder();
            LocalDate date = this.rates.keySet().stream()
                    .max(comparator)
                    .orElseThrow(() -> new MonetaryException("There is not more recent exchange rate to rate on BankeraRateProvider."));
            return new RateResult(this.rates.get(date));
        } else {
            for (LocalDate localDate : dates) {
                Map<String, ExchangeRate> targets = this.rates.get(localDate);

                if (Objects.nonNull(targets)) {
                    return new RateResult(targets);
                }
            }

            String datesOnErrors = Stream.of(dates).map(date -> date.format(DateTimeFormatter.ISO_LOCAL_DATE)).collect(Collectors.joining(","));
            throw new MonetaryException("There is not exchange on day " + datesOnErrors + " to rate to  rate on BankeraRateProvider.");
        }
    }

    private static class RateResult {

        private final Map<String, ExchangeRate> targets;

        RateResult(Map<String, ExchangeRate> targets) {
            this.targets = targets;
        }
    }

    private ExchangeRate createExchangeRate(ConversionQuery query, ExchangeRateBuilder builder, ExchangeRate sourceRate,
                                            ExchangeRate target) {
        if (areBothBaseCurrencies(query)) {
            builder.setFactor(DefaultNumberValue.ONE);
            return builder.build();
        } else if (BASE_CURRENCY_CODE.equals(query.getCurrency().getCurrencyCode())) {
            if (Objects.isNull(sourceRate)) {
                return null;
            }

            return reverse(sourceRate);
        } else if (BASE_CURRENCY_CODE.equals(query.getBaseCurrency().getCurrencyCode())) {
            return target;
        } else {
            ExchangeRate rate1 = getExchangeRate(
                    query.toBuilder().setTermCurrency(Monetary.getCurrency(BASE_CURRENCY_CODE)).build());
            ExchangeRate rate2 = getExchangeRate(
                    query.toBuilder().setBaseCurrency(Monetary.getCurrency(BASE_CURRENCY_CODE))
                            .setTermCurrency(query.getCurrency()).build());

            if (Objects.nonNull(rate1) && Objects.nonNull(rate2)) {
                builder.setFactor(multiply(rate1.getFactor(), rate2.getFactor()));
                builder.setRateChain(rate1, rate2);
                return builder.build();
            }

            throw new CurrencyConversionException(query.getBaseCurrency(),
                    query.getCurrency(), sourceRate.getContext());
        }
    }

    private boolean areBothBaseCurrencies(ConversionQuery query) {
        return BASE_CURRENCY_CODE.equals(query.getBaseCurrency().getCurrencyCode()) &&
                BASE_CURRENCY_CODE.equals(query.getCurrency().getCurrencyCode());
    }

    private ExchangeRateBuilder getBuilder(ConversionQuery query) {
        ExchangeRateBuilder builder = new ExchangeRateBuilder(getExchangeContext("bankera.digit.fraction"));
        builder.setBase(query.getBaseCurrency());
        builder.setTerm(query.getCurrency());
        return builder;
    }

    private ExchangeRate reverse(ExchangeRate rate) {
        if (Objects.isNull(rate)) {
            throw new IllegalArgumentException("Rate null is not reversible.");
        }

        return new ExchangeRateBuilder(rate).setRate(rate).setBase(rate.getCurrency()).setTerm(rate.getBaseCurrency())
                .setFactor(divide(DefaultNumberValue.ONE, rate.getFactor(), MathContext.DECIMAL64)).build();
    }
}
