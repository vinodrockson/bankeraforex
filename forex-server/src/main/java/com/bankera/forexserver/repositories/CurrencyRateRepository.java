package com.bankera.forexserver.repositories;

import com.bankera.forexserver.models.CurrencyRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

/**
 * Repository to handle CurrencyRate related data queries
 *
 * @author VinodJohn
 */
@Repository
public interface CurrencyRateRepository extends JpaRepository<CurrencyRate, UUID> {
    /**
     * To get latest list of Currency Rate of everyday since beginning
     *
     * @return list of Currency Rate
     */
    @Query(value = "SELECT c.* FROM currency_rate c " +
            "INNER JOIN " +
            "(SELECT MAX(created_date) max_time, currency_id FROM currency_rate " +
            "GROUP BY CAST(created_date AS DATE), currency_id) AS t " +
            "ON c.created_date = t.max_time and c.currency_id = t.currency_id",
            nativeQuery = true)
    List<CurrencyRate> findHistorically();

    @Query(value = "SELECT * FROM currency_rate " +
            "WHERE created_date IN " +
            "(SELECT MAX(created_date) " +
            "FROM currency_rate " +
            "GROUP BY currency_id)",
            nativeQuery = true)
    List<CurrencyRate> findAllLatest();
}
