package com.bankera.forexserver.repositories;

import com.bankera.forexserver.models.BfxCurrency;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

/**
 * Repository to handle Currency related data queries
 *
 * @author Vinod John
 */
public interface CurrencyRepository extends JpaRepository<BfxCurrency, UUID> {
    Optional<BfxCurrency> findByCurrencyCode(String currencyCode);
}
