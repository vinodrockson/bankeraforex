package com.bankera.forexserver.repositories;

import com.bankera.forexserver.models.ExternalSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Repository to handle ExternalSession data queries
 *
 * @author VinodJohn
 */
@Repository
public interface ExternalSessionRepository extends JpaRepository<ExternalSession, UUID> {
}
