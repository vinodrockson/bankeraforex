package com.bankera.forexserver.services.currency;

import com.bankera.forexserver.exceptions.CurrencyNotFoundException;
import com.bankera.forexserver.models.BfxCurrency;

import java.util.List;
import java.util.UUID;

/**
 * Service to handle Currency business logic and operations
 *
 * @author Vinod John
 */
public interface CurrencyService {
    /**
     * To create a new currency, if not exists
     *
     * @param currency Currency
     * @return Currency
     */
    BfxCurrency createCurrency(BfxCurrency currency);

    /**
     * To get list of currencies
     *
     * @return List of currencies
     */
    List<BfxCurrency> getAllCurrencies();

    /**
     * To get list of active currencies
     *
     * @return List of currencies
     */
    List<BfxCurrency> getActiveCurrencies();

    /**
     * To check whether the given currency is active
     *
     * @return True or False
     */
    boolean isCurrencyActive(String currencyCode) throws CurrencyNotFoundException;

    /**
     * To delete currency (change active status) by ID
     *
     * @param id Currency ID
     */
    void deleteCurrencyById(UUID id) throws CurrencyNotFoundException;

    /**
     * To restore currency (change active status) by ID
     *
     * @param id Currency ID
     */
    void restoreCurrencyById(UUID id) throws CurrencyNotFoundException;
}
