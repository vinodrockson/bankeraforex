package com.bankera.forexserver.services.currency;

import com.bankera.forexserver.exceptions.CurrencyNotFoundException;
import com.bankera.forexserver.models.BfxCurrency;
import com.bankera.forexserver.repositories.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Currency;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.bankera.forexserver.utils.Constants.BankeraCurrency.CURRENCY_NAME;
import static com.bankera.forexserver.utils.Constants.BankeraCurrency.CURRENCY_SYMBOL;

/**
 * Implementation of Currency Service
 *
 * @author Vinod John
 */
@Service
@Transactional
public class CurrencyServiceImpl implements CurrencyService {
    @Autowired
    private CurrencyRepository currencyRepository;

    @Override
    public BfxCurrency createCurrency(BfxCurrency currency) {
        try {
            return findByCurrencyCode(currency.getCurrencyCode());
        } catch (CurrencyNotFoundException cnfe) {
            String currencyName, currencySymbol;

            try {
                currencyName = Currency.getInstance(currency.getCurrencyCode()).getDisplayName();
                currencySymbol = Currency.getInstance(currency.getCurrencyCode()).getSymbol();
            } catch (Exception e) {
                currencyName = !CURRENCY_NAME.get(currency.getCurrencyCode()).isBlank() ?
                        CURRENCY_NAME.get(currency.getCurrencyCode()) : currency.getCurrencyCode();
                currencySymbol = !CURRENCY_SYMBOL.get(currency.getCurrencyCode()).isBlank() ?
                        CURRENCY_SYMBOL.get(currency.getCurrencyCode()) : currency.getCurrencyCode();
            }

            currency.setCurrencyName(currencyName);
            currency.setCurrencySymbol(currencySymbol);
            currency.setActive(true);
            return currencyRepository.saveAndFlush(currency);
        }
    }

    @Override
    public List<BfxCurrency> getAllCurrencies() {
        return currencyRepository.findAll();
    }

    @Override
    public List<BfxCurrency> getActiveCurrencies() {
        return getAllCurrencies().stream()
                .filter(BfxCurrency::isActive)
                .collect(Collectors.toList());
    }

    @Override
    public boolean isCurrencyActive(String currencyCode) throws CurrencyNotFoundException {
        return findByCurrencyCode(currencyCode).isActive();
    }

    @Override
    public void deleteCurrencyById(UUID id) throws CurrencyNotFoundException {
        BfxCurrency currency = findById(id);
        if (currency.isActive()) {
            currency.setActive(false);
            currencyRepository.saveAndFlush(currency);
        }
    }

    @Override
    public void restoreCurrencyById(UUID id) throws CurrencyNotFoundException {
        BfxCurrency currency = findById(id);
        if (!currency.isActive()) {
            currency.setActive(true);
            currencyRepository.saveAndFlush(currency);
        }
    }

    // PRIVATE METHODS //
    private BfxCurrency findById(UUID id) throws CurrencyNotFoundException {
        return currencyRepository.findById(id)
                .orElseThrow(() -> new CurrencyNotFoundException(id));
    }

    private BfxCurrency findByCurrencyCode(String currencyCode) throws CurrencyNotFoundException {
        return currencyRepository.findByCurrencyCode(currencyCode)
                .orElseThrow(() -> new CurrencyNotFoundException(currencyCode));
    }

}
