package com.bankera.forexserver.services.currencyrate;

import com.bankera.forexserver.models.CurrencyRate;

import java.util.List;

/**
 * Service to handle CurrencyRate business logic and operations
 *
 * @author Vinod John
 */
public interface CurrencyRateService {
    /**
     * To create Currency Rates in bulk
     *
     * @param currencyRates list of currency rates
     */
    void createCurrencyRate(List<CurrencyRate> currencyRates);

    /**
     * To get history of currency rate
     *
     * @return Currency Rate list
     */
    List<CurrencyRate> getCurrencyRateHistory();

    /**
     * To get latest currency rate of all available currencies
     *
     * @return list of Currency Rate
     */
    List<CurrencyRate> getLatestRateOfAvailableCurrencies();
}
