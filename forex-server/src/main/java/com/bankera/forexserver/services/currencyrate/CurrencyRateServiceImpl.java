package com.bankera.forexserver.services.currencyrate;

import com.bankera.forexserver.dtos.CurrencyRateDto;
import com.bankera.forexserver.models.CurrencyRate;
import com.bankera.forexserver.repositories.CurrencyRateRepository;
import com.bankera.forexserver.services.currency.CurrencyService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementation of CurrencyRate service
 *
 * @author Vinod John
 */
@Service
@Transactional
public class CurrencyRateServiceImpl implements CurrencyRateService {
    @Autowired
    private CurrencyRateRepository currencyRateRepository;

    @Autowired
    private CurrencyService currencyService;

    @Override
    public void createCurrencyRate(List<CurrencyRate> currencyRates) {
        currencyRates.forEach(currencyRate -> currencyRate.setCurrency(currencyService.createCurrency(currencyRate.getCurrency())));
        currencyRateRepository.saveAll(currencyRates);
        writeCurrencyRateJson();
    }

    @Override
    public List<CurrencyRate> getCurrencyRateHistory() {
        return currencyRateRepository.findHistorically();
    }

    @Override
    public List<CurrencyRate> getLatestRateOfAvailableCurrencies() {
        return currencyRateRepository.findAllLatest();
    }

    // PRIVATE METHODS //
    private void writeCurrencyRateJson() {
        try {
            List<CurrencyRateDto> currencyRates = getLatestRateOfAvailableCurrencies()
                    .stream()
                    .map(CurrencyRate::currencyRateDto)
                    .collect(Collectors.toList());
            Writer writer = new FileWriter("forex-server/src/main/resources/bankera-rates.json");
            Gson gson = new GsonBuilder().create();
            gson.toJson(currencyRates, writer);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
