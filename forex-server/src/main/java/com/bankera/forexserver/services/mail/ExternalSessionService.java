package com.bankera.forexserver.services.mail;

import com.bankera.forexserver.exceptions.ExternalSessionNotFoundException;
import com.bankera.forexserver.models.ExternalSession;

import java.util.UUID;

/**
 * Service to handle ExternalSession related operations
 *
 * @author VinodJohn
 */
public interface ExternalSessionService {

    /**
     * To create new external session
     *
     * @param externalSession ExternalSession
     * @return URL of session
     */
    String createNewSession(ExternalSession externalSession);

    /**
     * To get a session by ID
     *
     * @param id ExternalSession ID
     * @return ExternalSession
     */
    ExternalSession getSessionById(UUID id) throws ExternalSessionNotFoundException;


    /**
     * To delete existing external session
     *
     * @param externalSession ExternalSession
     */
    void deleteSession(ExternalSession externalSession);
}
