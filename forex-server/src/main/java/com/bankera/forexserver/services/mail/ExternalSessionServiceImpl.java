package com.bankera.forexserver.services.mail;

import com.bankera.forexserver.exceptions.ExternalSessionNotFoundException;
import com.bankera.forexserver.models.ExternalSession;
import com.bankera.forexserver.repositories.ExternalSessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 * Implementation of ExternalSession service
 *
 * @author VinodJohn
 */
@Service
@Transactional
public class ExternalSessionServiceImpl implements ExternalSessionService {
    @Autowired
    private ExternalSessionRepository externalSessionRepository;

    @Override
    public String createNewSession(ExternalSession externalSession) {
        externalSession = externalSessionRepository.saveAndFlush(externalSession);
        return externalSession.getUrl().concat("/").concat(externalSession.getId().toString());
    }

    @Override
    public ExternalSession getSessionById(UUID id) throws ExternalSessionNotFoundException {
        return externalSessionRepository.findById(id)
                .orElseThrow(ExternalSessionNotFoundException::new);
    }

    @Override
    public void deleteSession(ExternalSession externalSession) {
        externalSession.setExpired(true);
        externalSessionRepository.saveAndFlush(externalSession);
    }
}
