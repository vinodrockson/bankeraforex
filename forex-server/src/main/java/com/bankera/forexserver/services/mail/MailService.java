package com.bankera.forexserver.services.mail;


import com.bankera.forexserver.models.Email;

import javax.mail.MessagingException;


/**
 * Service is to send emails
 *
 * @author VinodJohn
 */
public interface MailService {
    /**
     * To send an email
     *
     * @param email Email
     */
    void sendEmail(Email email) throws MessagingException;
}
