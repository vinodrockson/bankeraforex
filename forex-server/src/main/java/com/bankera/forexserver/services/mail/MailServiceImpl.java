package com.bankera.forexserver.services.mail;


import com.bankera.forexserver.models.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Map;

@Service
public class MailServiceImpl implements MailService {
    @Autowired
    private JavaMailSender javaMailSender;

    @Override
    public void sendEmail(Email email) throws MessagingException {
        MimeMessage msg = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
        helper.setTo(email.getRecipient());
        helper.setSubject(email.getSubject());
        helper.setText(email.getMessage().concat(composeLinks(email.getLinks())), true);
        javaMailSender.send(msg);
    }

    // PRIVATE METHODS //
    private String composeLinks(Map<String, String> links) {
        StringBuilder result = new StringBuilder("<br/><br/>");

        for (Map.Entry<String, String> entry : links.entrySet()) {
            result.append("<h5><a href='").append(entry.getValue()).append("' target='_blank'>").append(entry.getKey()).append("</a></h5><br/><br/>");
        }

        return result.toString();
    }
}
