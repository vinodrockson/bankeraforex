package com.bankera.forexserver.services.user;

import com.bankera.forexserver.exceptions.ExternalSessionExpiredException;
import com.bankera.forexserver.exceptions.ExternalSessionNotFoundException;
import com.bankera.forexserver.exceptions.UserNotFoundException;
import com.bankera.forexserver.models.User;

import java.util.List;
import java.util.UUID;

/**
 * Service to handle User business logic and operations
 *
 * @author VinodJohn
 */
public interface UserService {

    /**
     * To create a new User
     *
     * @param user User
     */
    void createUser(User user);

    /**
     * To update an existing User
     *
     * @param user user
     */
    void updateUser(User user) throws UserNotFoundException;

    /**
     * To get user by ID
     *
     * @param id User ID
     * @return User
     */
    User getUserById(UUID id) throws UserNotFoundException;

    /**
     * To get user by email
     *
     * @param email User email
     * @return User
     */
    User getUserByEmail(String email) throws UserNotFoundException;

    /**
     * To get all the users
     *
     * @return list of all users
     */
    List<User> getAllUsers();

    /**
     * Delete user(change active status) by ID
     *
     * @param id User ID
     */
    void deleteUserById(UUID id) throws UserNotFoundException;

    /**
     * Restore user(change active state) by ID
     *
     * @param id User ID
     */
    void restoreUserById(UUID id) throws UserNotFoundException;

    /**
     * Activate user by session ID
     *
     * @param sessionId Session ID
     */
    void activateUser(UUID sessionId) throws ExternalSessionNotFoundException, ExternalSessionExpiredException;

    /**
     * Reset password for existing user by email
     *
     * @param userEmail User email
     */
    void resetUserPasswordByEmail(String userEmail) throws UserNotFoundException;

    /**
     * Reset password for existing user
     *
     * @param sessionId SessionId to reset password
     * @param password  new password for user
     */
    void resetUserPassword(UUID sessionId, String password) throws ExternalSessionNotFoundException, ExternalSessionExpiredException;
}

