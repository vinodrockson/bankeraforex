package com.bankera.forexserver.services.user;

import com.bankera.forexserver.exceptions.ExternalSessionExpiredException;
import com.bankera.forexserver.exceptions.ExternalSessionNotFoundException;
import com.bankera.forexserver.exceptions.UserNotFoundException;
import com.bankera.forexserver.models.*;
import com.bankera.forexserver.repositories.UserRepository;
import com.bankera.forexserver.services.mail.ExternalSessionService;
import com.bankera.forexserver.services.mail.MailService;
import com.bankera.forexserver.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.time.LocalDateTime;
import java.util.*;

import static com.bankera.forexserver.utils.Constants.Client.*;


/**
 * Implementation of User Service
 *
 * @author VinodJohn
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ExternalSessionService externalSessionService;

    @Autowired
    private MailService mailService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public void createUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        if (null == user.getAuthority()) {
            user.setAuthority(Authority.ROLE_USER.name());
        }

        user = userRepository.saveAndFlush(user);

        try {
            if (Authority.ROLE_USER.name().equals(user.getAuthority())) {
                mailService.sendEmail(getActivationEmail(user));
            }
        } catch (MessagingException e) {
            throw new RuntimeException("{messages.client.email-error}");
        }
    }

    @Override
    public void updateUser(User user) throws UserNotFoundException {
        if (!userRepository.existsById(user.getId())) {
            throw new UserNotFoundException(Objects.requireNonNull(user).getId());
        }

        String encodedPassword = passwordEncoder.encode(user.getPassword());
        if (!user.getPassword().equals(encodedPassword)) {
            user.setPassword(encodedPassword);
        }

        if (null == user.getAuthority()) {
            user.setAuthority(Authority.ROLE_USER.name());
        }

        userRepository.saveAndFlush(user);
    }

    @Override
    public User getUserById(UUID id) throws UserNotFoundException {
        return userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));
    }

    @Override
    public User getUserByEmail(String email) throws UserNotFoundException {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException(email));
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void deleteUserById(UUID id) throws UserNotFoundException {
        User user = getUserById(id);
        user.setActive(false);
        userRepository.saveAndFlush(user);

        Email email = new Email();
        email.setRecipient(user.getEmail());
        email.setSubject("Account deleted - Bankera Forex");
        email.setMessage("<h1>Goodbye!</h1><p>Thank you for using our service!</p>");

        try {
            mailService.sendEmail(email);
        } catch (MessagingException e) {
            throw new RuntimeException("{messages.client.email-error}");
        }
    }

    @Override
    public void restoreUserById(UUID id) throws UserNotFoundException {
        User user = getUserById(id);
        user.setActive(true);
        userRepository.saveAndFlush(user);

        Map<String, String> links = new HashMap<>();
        links.put("Click here to login", Constants.Client.BASE_URL);

        Email email = new Email();
        email.setRecipient(user.getEmail());
        email.setSubject("Account recovered - Bankera Forex");
        email.setMessage("<h1>Hooray!</h1><p>Your account has been restored!</p>");
        email.setLinks(links);

        try {
            mailService.sendEmail(email);
        } catch (MessagingException e) {
            throw new RuntimeException("{messages.client.email-error}");
        }
    }

    @Override
    public void activateUser(UUID sessionId) throws ExternalSessionNotFoundException, ExternalSessionExpiredException {
        ExternalSession externalSession = externalSessionService.getSessionById(sessionId);

        if (LocalDateTime.now().isAfter(externalSession.getExpiryDate())) {
            externalSessionService.deleteSession(externalSession);

            try {
                mailService.sendEmail(getActivationEmail(externalSession.getUser()));
            } catch (MessagingException e) {
                throw new RuntimeException("{messages.client.email-error}");
            }

            throw new ExternalSessionExpiredException();
        }

        User user = externalSession.getUser();
        user.setEmailActivated(true);
        user.setActive(true);
        userRepository.saveAndFlush(user);
        externalSessionService.deleteSession(externalSession);
    }

    @Override
    public void resetUserPasswordByEmail(String userEmail) {
        try {
            User user = getUserByEmail(userEmail);
            mailService.sendEmail(getResetPasswordEmail(user));
        } catch (MessagingException | UserNotFoundException e) {
            throw new RuntimeException("{messages.client.email-error}");
        }
    }

    @Override
    public void resetUserPassword(UUID sessionId, String password) throws ExternalSessionNotFoundException, ExternalSessionExpiredException {
        ExternalSession externalSession = externalSessionService.getSessionById(sessionId);

        if (LocalDateTime.now().isAfter(externalSession.getExpiryDate())) {
            externalSessionService.deleteSession(externalSession);

            try {
                mailService.sendEmail(getResetPasswordEmail(externalSession.getUser()));
            } catch (MessagingException e) {
                throw new RuntimeException("{messages.client.email-error}");
            }

            throw new ExternalSessionExpiredException();
        }

        User user = externalSession.getUser();
        user.setPassword(passwordEncoder.encode(password));
        userRepository.saveAndFlush(user);
        externalSessionService.deleteSession(externalSession);
    }

    // PRIVATE METHODS //
    private Email getActivationEmail(User user) {
        ExternalSession externalSession = new ExternalSession();
        externalSession.setUser(user);
        externalSession.setExternalSessionType(ExternalSessionType.CREATE_USER);
        externalSession.setUrl(Constants.Client.BASE_URL.concat(Constants.Client.USER_ACTIVATION_URL));
        externalSession.setExpiryDate(LocalDateTime.now().plusHours(USER_ACTIVATION_URL_LIFE_HOURS));
        externalSession.setExpired(false);

        Map<String, String> links = new HashMap<>();
        links.put("Click here to activate your account", externalSessionService.createNewSession(externalSession));

        Email email = new Email();
        email.setRecipient(user.getEmail());
        email.setSubject("Activation email - Bankera Forex");
        email.setMessage("<h1>Welcome to Bankera Forex</h1><p>Thank you for registering with us." +
                "The below link will be active for next " + USER_ACTIVATION_URL_LIFE_HOURS + " hours.</p>");
        email.setLinks(links);

        return email;
    }

    private Email getResetPasswordEmail(User user) {
        ExternalSession externalSession = new ExternalSession();
        externalSession.setUser(user);
        externalSession.setExternalSessionType(ExternalSessionType.RESET_USER_PASSWORD);
        externalSession.setUrl(BASE_URL.concat(RESET_PASSWORD_URL));
        externalSession.setExpiryDate(LocalDateTime.now().plusMinutes(Constants.Client.RESET_PASSWORD_LIFE_MINUTES));
        externalSession.setExpired(false);

        Map<String, String> links = new HashMap<>();
        links.put("Click here to reset password for your account", externalSessionService.createNewSession(externalSession));

        Email email = new Email();
        email.setRecipient(user.getEmail());
        email.setSubject("Password reset - Bankera Forex");
        email.setMessage("<h1>Reset password</h1><p>The below link will be active for next " +
                Constants.Client.RESET_PASSWORD_LIFE_MINUTES + " minutes.</p>");
        email.setLinks(links);

        return email;
    }
}
