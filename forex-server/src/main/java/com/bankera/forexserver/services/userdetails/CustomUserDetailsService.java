package com.bankera.forexserver.services.userdetails;

import com.bankera.forexserver.models.User;
import com.bankera.forexserver.services.user.UserService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

/**
 * Customized implementation UserDetailsService
 *
 * @author VinodJohn
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserService userService;

    @Override
    @SneakyThrows
    public UserDetails loadUserByUsername(String email) {
        User user = userService.getUserByEmail(email);
        return new CustomUserDetails(user);
    }
}
