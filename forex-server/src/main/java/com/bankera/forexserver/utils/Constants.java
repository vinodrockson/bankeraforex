package com.bankera.forexserver.utils;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.util.Map;

/**
 * Constant values used in this application
 *
 * @author VinodJohn
 */
public class Constants {
    public static class Security {
        public static final String DEFAULT_USER_ADMIN_EMAIL = "admin@bfx.com";
        public static final String DEFAULT_USER_ADMIN_PASSWORD = "bfxadmin";
    }

    public static class Audit {
        public static final String DEFAULT_AUDITOR = "SYSTEM";
    }

    public static class BankeraCurrency {
        public static final String BASE_CURRENCY_CODE = "EUR";
        public static final CurrencyUnit BASE_CURRENCY = Monetary.getCurrency(BASE_CURRENCY_CODE);

        public static final String BTC = "BTC";
        public static final String ETH = "ETH";
        public static final String FKE = "FKE";

        public static final Map<String, String> CURRENCY_NAME = Map.of(BTC, "Bitcoin", ETH, "Ethereum", FKE, "FKE");
        public static final Map<String, String> CURRENCY_SYMBOL = Map.of(BTC, "₿", ETH, "Ξ", FKE, "FKE");
    }

    public static class Client {
        public static final String BASE_URL = "http://localhost:4200";
        public static final String USER_ACTIVATION_URL = "/auth/activate-user";
        public static final String RESET_PASSWORD_URL = "/auth/reset-password";
        public static final int USER_ACTIVATION_URL_LIFE_HOURS = 5;
        public static final int RESET_PASSWORD_LIFE_MINUTES = 10;
    }
}
