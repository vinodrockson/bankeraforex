package com.bankera.forexserver.utils;

import com.bankera.forexserver.providers.BankeraRateProvider;
import org.javamoney.moneta.OSGIServiceHelper;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import javax.money.convert.ExchangeRateProvider;
import java.util.logging.Logger;

/**
 * An activator that registers OSGi services
 *
 * @author Vinod John
 */
public class OSGIActivator implements BundleActivator {
    private static final Logger LOG = Logger.getLogger(OSGIActivator.class.getName());

    @Override
    public void start(BundleContext bundleContext) {
        LOG.info("Registering Bankera Rate Provider service...");
        OSGIServiceHelper.registerService(bundleContext.getBundle(), ExchangeRateProvider.class, BankeraRateProvider.class);
        LOG.info("Registered Bankera Rate Provider service...");
    }

    @Override
    public void stop(BundleContext bundleContext) {
        LOG.info("Unregistering Bankera Rate Provider service...");
        OSGIServiceHelper.unregisterService(bundleContext.getBundle(), ExchangeRateProvider.class, BankeraRateProvider.class);
        LOG.info("Unregistered Bankera Rate Provider service...");
    }
}
