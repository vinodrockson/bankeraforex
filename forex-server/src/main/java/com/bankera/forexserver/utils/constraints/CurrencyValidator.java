package com.bankera.forexserver.utils.constraints;

import com.bankera.forexserver.exceptions.CurrencyNotFoundException;
import com.bankera.forexserver.services.currency.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Constraint validator to check if currency is active
 *
 * @author Vinod John
 */
public class CurrencyValidator implements ConstraintValidator<ValidCurrency, String> {
    @Autowired
    private CurrencyService currencyService;

    @Override
    public void initialize(ValidCurrency constraintAnnotation) {

    }

    @Override
    public boolean isValid(String currencyCode, ConstraintValidatorContext constraintValidatorContext) {
        try {
            if (!currencyService.isCurrencyActive(currencyCode)) {
                throw new RuntimeException("Currency (code: " + currencyCode + ") is inactive! Exchange cannot be performed.");
            }

            return true;
        } catch (RuntimeException | CurrencyNotFoundException e) {
            if (!e.getMessage().isBlank()) {
                constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                        .addConstraintViolation()
                        .disableDefaultConstraintViolation();
            }
        }

        return false;
    }
}
