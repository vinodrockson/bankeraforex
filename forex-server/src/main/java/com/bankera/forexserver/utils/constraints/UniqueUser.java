package com.bankera.forexserver.utils.constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Constraint annotation for unique user validation
 *
 * @author VinodJohn
 */
@Documented
@Target({ElementType.ANNOTATION_TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE})
@Constraint(validatedBy = {UniqueUserValidator.class})
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueUser {
    String message() default "{messages.constraints.unique-user}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
