package com.bankera.forexserver.utils.constraints;

import com.bankera.forexserver.exceptions.UserNotFoundException;
import com.bankera.forexserver.models.User;
import com.bankera.forexserver.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Constraint validator to check user's email already exists and valid Authority
 *
 * @author VinodJohn
 */
public class UniqueUserValidator implements ConstraintValidator<UniqueUser, User> {
    @Autowired
    private UserService userService;

    @Override
    public void initialize(UniqueUser constraintAnnotation) {
    }

    @Override
    public boolean isValid(User user, ConstraintValidatorContext constraintValidatorContext) {
        try {
            return null != user.getId() && null != userService.getUserByEmail(user.getEmail());
        } catch (RuntimeException | UserNotFoundException e) {
            if (!e.getMessage().isBlank()) {
                constraintValidatorContext.buildConstraintViolationWithTemplate(e.getMessage())
                        .addConstraintViolation()
                        .disableDefaultConstraintViolation();
            }

        }

        return false;
    }
}
