package com.bankera.forexserver.utils.constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Constraint annotation for currency validation
 *
 * @author Vinod John
 */
@Documented
@Target({ElementType.ANNOTATION_TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE})
@Constraint(validatedBy = {CurrencyValidator.class})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidCurrency {
    String message() default "{messages.constraints.inactive-currency}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
